from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Shoe, BinVO

from common.json import ModelEncoder

# 3 encoders
class BinVOEncoder(ModelEncoder):
    model=BinVO
    properties=["import_href",
     "name",]


class ShoeEncoder(ModelEncoder):
    model=Shoe
    properties=[
    "manufacturer",
    "model",
    "id",
    ]

class ShoeDetailEncoder(ModelEncoder):
    model=Shoe
    properties=[
    "manufacturer",
    "model",
    "color",
    "picture_url",
    "bin",
    "id",
    ]
    encoders={
      "bin": BinVOEncoder()
  }

#encoders must be run as functions



@require_http_methods(["GET","POST"])
def list_shoes(request):
    if request.method == "GET":
        shoes=Shoe.objects.all()
        return JsonResponse (
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else: content = json.loads(request.body)

    bin=BinVO.objects.get(import_href=content["bin"])
    #get filters
    content["bin"]=bin

    shoes = Shoe.objects.create(**content)
    return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET","PUT","DELETE"])
def shoe_details(request,id):
    if request.method =="GET":
        shoes=Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            #stop thinking we're using the model here^
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method =="DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count>0})
    else:
        content=json.loads(request.body)

        bin=BinVO.objects.get(import_href=content["bin"])
        content["bin"]=bin
        #has to be before spread of content
        Shoe.objects.filter(id=id).update(**content)
        shoes=Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

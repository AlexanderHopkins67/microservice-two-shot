from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.name}"
class Shoe(models.Model):
    manufacturer= models.CharField(max_length=40)
    model= models.CharField(max_length=40)
    color= models.CharField(max_length=40)
    picture_url= models.URLField()
    bin= models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.PROTECT


    )

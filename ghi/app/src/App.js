import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatDetails from './HatDetails';
import ShoeDetails from './ShoeDetails';
import NewHat from './NewHat';
import NewShoe from './NewShoe';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatDetails />} />
          <Route path="shoes" element={<ShoeDetails />} />
          <Route path="hats/new" element={<NewHat />} />
          <Route path="shoes/new" element={<NewShoe />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

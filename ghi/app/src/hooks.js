import { useEffect, useState } from "react"

export function useDefaultState() {
    const [state, setState] = useState("")

    function handleChange(event) {
        event.preventDefault()
        setState(event.target.value)
    }

    function reset() {
        setState("")
    }

    return {"state": state, "handleChange": handleChange, "reset": reset}
}


export function useListState(){
    const [list, setList] = useState([])

    function initializeList(data) {
        setList(data)
    }

    return {"list": list, "setList": initializeList}
}

//this did not work worth a shit, it can fucking rot in hell
//...
//gave up on this solution, came up with a better one
// export function useCardState() {
//     const [cards, setCards] = useState([])
//     const [cardData, setCardData] = useState([])

//     async function deleteCard(event) {
//         console.log("fired", cardData)
//         const id = event.target.value
//         const fetchConfig = {
//             method: "delete",
//             headers: {
//                 "Content-Type": 'application/json',
//             }
//         }
//         const response = await fetch(`http://localhost:8090/api/hats/${event.target.value}/`, fetchConfig)
//         if (response.ok) {
//             const copyData = [...cardData]
//             setCards(copyData.filter((card) => card.id != id))
//             console.log(cardData)
//         }

//     }

//     function addData(details) {
//         setCardData(details)
//     }

//     function createCards() {
//         let data = []
//         cardData.forEach((detail) => {
//             data.push(
//                     <div className="card shadow" style={{width: "33%", display: "flex", justifyContent: "center", margin: "1rem"}}>
//                         <img src={detail.img_url} className="card-img-top" />
//                         <footer className="card-footer bg-white footer-position" style={{
//                             display: "flex",
//                             flexDirection: "row",
//                             columnGap: ".5rem",
//                             minHeight: "4rem",
//                             }}>
//                             <div>
//                                 <p className="card-title fw-bold text-secondary">{detail.name}:</p>
//                                 <p className="card-text text-muted text-secondary">{detail.color} {detail.fabric.name} {detail.style.name}</p>
//                             </div>
//                             <div>
//                                 <button className="btn btn-danger" onClick={deleteCard} value={detail.id} style={{position: "absolute", right: 0.5, margin: "5px"}}>Delete</button>
//                             </div>
//                         </footer>
//                     </div>
//             )
//         })

//         setCards(data)
//     }

//     useEffect(() => {
//         createCards()
//     }, [cardData])


//     return {"cards": cards, "addData": addData}
// }
import {useEffect} from "react"
import { useDefaultState, useListState } from "./hooks"

export default function NewHat() {
    const locationList = useListState()
    const styleList = useListState()
    const fabricList = useListState()
    const name = useDefaultState()
    const location = useDefaultState()
    const style = useDefaultState()
    const fabric = useDefaultState()
    const color = useDefaultState()
    const img = useDefaultState()

    const fetchData = async () => {
        const locations = await fetch("http://localhost:8090/api/locations/")
        const styles = await fetch("http://localhost:8090/api/styles/")
        const fabrics = await fetch("http://localhost:8090/api/fabrics/")

        if (locations.ok && styles.ok && fabrics.ok) {
            const locationData = await locations.json()
            const styleData = await styles.json()
            const fabricData = await fabrics.json()

            locationList.setList(locationData.locations)
            styleList.setList(styleData.styles)
            fabricList.setList(fabricData.fabrics)

        } else {
            console.log("Fetch Failed")
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {
            "name": name.state,
            "location": location.state,
            "style": style.state,
            "fabric": fabric.state,
            "color": color.state,
            "img_url": img.state,
        }

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch("http://localhost:8090/api/hats/", fetchConfig)
        if (response.ok) {
            name.reset()
            location.reset()
            style.reset()
            fabric.reset()
            color.reset()
            img.reset()
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new hat to the closet</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input value={name.state} onChange={name.handleChange} placeholder="Hat Name" required type="text" name="hat-name" id="hat-name" className="form-control"/>
                <label htmlFor="hat-name">Hat Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color.state} onChange={color.handleChange} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={img.state} onChange={img.handleChange} placeholder="hat-img" type="url" name="hat-img" id="hat-img" className="form-control"/>
                <label htmlFor="hat-img">Hat Image URL</label>
              </div>
              <div className="mb-3">
                <select value={style.state} onChange={style.handleChange} required name="style" id="style" className="form-select">
                  <option key="defaultPres" value="">Choose a Style</option>
                  {styleList.list.map(data => <option key={data.id} value={data.name}>{data.name}</option>)}
                </select>
              </div>
              <div className="mb-3">
                <select value={fabric.state} onChange={fabric.handleChange} required name="fabric" id="fabric" className="form-select">
                  <option key="defaultPres" value="">Choose a Fabric</option>
                  {fabricList.list.map(data => <option key={data.id} value={data.name}>{data.name}</option>)}
                </select>
              </div>
              <div className="mb-3">
                <select value={location.state} onChange={location.handleChange} required name="location" id="location" className="form-select">
                  <option key="defaultPres" value="">Choose a Location</option>
                  {locationList.list.map(data => <option key={data.import_href} value={data.import_href}>{data.name}</option>)}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
    </div>
    )
}
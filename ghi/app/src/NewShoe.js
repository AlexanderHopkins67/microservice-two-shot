import {useEffect, useState} from "react"

 function NewShoe(props) {
    const[bins,setBins]=useState([]);
    const[manufacturer,SetManufacturer]=useState('');
    const[model,SetModel]=useState('');
    const[color,SetColor]=useState('');
    const[image,SetImage]=useState('');
    const[bin,SetBin]=useState('');

    const handleManufacturerChange=(event)=>{
        const value=event.target.value;
        SetManufacturer(value);
    }
    const handleModelChange=(event)=>{
        const value=event.target.value;
        SetModel(value);
    }
    const handleColorChange=(event)=>{
        const value=event.target.value;
        SetColor(value);
    }
    const handleImageChange=(event)=>{
        const value=event.target.value;
        SetImage(value);
    }
    const handleBinChange=(event)=>{
        const value=event.target.value;
        SetBin(value);


    }

    const handleSubmit= async (event)=>{
        event.preventDefault();
        const data={};

        data.manufacturer= manufacturer;
        data.model=model;
        data.color=color;
        data.picture_url=image;
        data.bin=bin;

        console.log(data);



        const ShoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
           method: 'post',
           body: JSON.stringify(data),
           headers: {
             'Content-Type': 'application/json',
           },
         };

         const ShoeResponse = await fetch(ShoeUrl, fetchOptions);
         if (ShoeResponse.ok) {
           SetManufacturer('');
           SetModel('');
           SetColor('');
           SetImage('');
           SetBin('');

         }

        }





        const fetchData= async()=>{
            const url='http://localhost:8100/api/bins/';
            // use the URL associated with browsers and insomnia

            const response = await fetch(url);

            if (response.ok) {
              const data = await response.json();
              setBins(data.bins)
              console.log(data.bins)


        }
    }



    useEffect(() => {
        fetchData();
    }, []);

        return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form  onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input value={model} onChange={handleModelChange} placeholder="Model" required type="text" name="model" id="model" className="form-control"/>
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={image} onChange={handleImageChange} placeholder="Image URL" required type="url" name="image" id="image" className="form-control"/>
                <label htmlFor="image">Image</label>
              </div>
              <div className="mb-3">
                <select  value={bin}onChange={handleBinChange}required id="bins" name="bins" className="form-select">
                  <option  value="">Choose a Bin</option>
                  {bins.map(bin => {
                     return (
                     <option  key={bin.href} value={bin.href}>
                         {bin.closet_name}
                     </option>
                     );
                 })}
                 </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );

}




export default NewShoe;

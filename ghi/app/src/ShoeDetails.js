import {useEffect, useState} from "react"
import { Link } from "react-router-dom";

 function ShoeDetails(props) {

const[shoeList,setShoes]=useState([])


async function loadShoeDetails(id) {
    const response = await fetch(`http://localhost:8080/api/shoes/${id}/`);
    if (response.ok) {
      const data = await response.json();
      return data
    //   what to write there^
    } else {
      console.error(response);
    }
  }

async function loadShoeList() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const data = await response.json();
        let shoeList= []
        for (let shoe of data.shoes){
            const result = loadShoeDetails(shoe.id)
            shoeList.push(result)
        }
        const promiseList=await Promise.all(shoeList)
        setShoes(promiseList)




        //   what to write there^
        // use each shoe from lsit and using id load each detail and fill a list with it
    } else {
      console.error(response);
    }
  }

  async function deleteShoe(event){
    let id=event.target.value
    const deleteURL = `http://localhost:8080/api/shoes/${id}/`;
    console.log(id);
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      }



    }


const shoeResponse = await fetch(deleteURL, fetchOptions);
if (shoeResponse.ok) {
    const copyDetails=[...shoeList]
    setShoes(copyDetails.filter((shoe)=>shoe.id != id))
//  you need a second set of parantheses for filter so .filter((x)=> x.y conditional)

  }
  }






useEffect(() => {
    loadShoeList()
}, []);

//   delete from backend and frontend




    return (
         <div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Shoe</th>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Color</th>
              <th>Image</th>
              <th>Bin </th>
            </tr>
          </thead>
          <tbody>
            {shoeList.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td >{ shoe.manufacturer }</td>
                  <td>{ shoe.model}</td>
                  <td>{ shoe.color }</td>
                  <td><img  style={{width : "50px"}} src={shoe.picture_url}/></td>
                  <td>{ shoe.bin.name}</td>
                  <td><button onClick={deleteShoe} value={shoe.id}>Delete </button> </td>


                </tr>



                 );
            })}
          </tbody>
        </table>
        <div className="col-lg-6 mx-auto">
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a shoe!</Link>
                    </div>
                </div>

        </div>




      );
}

// delete the shoe somehow


 export default ShoeDetails;

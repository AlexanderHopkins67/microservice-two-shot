import {useEffect, useState} from "react"
import { Link } from 'react-router-dom';
import { useCardState } from "./hooks";
import './index.css'


function DetailCards(details, deleteCard) {
    let response = []
    for (let i = 0; i < details.length; i+=3) {
        const divClass = response.length === 0 ? "carousel-item active" : "carousel-item"
        response.push(
            <div className={divClass}>
                <div className="cards-group">
                    {details.slice(i, i+3).map((detail) => {
                        return(
                            <div key={detail.id} className="card shadow h-100" style={{width: "33%", display: "flex", justifyContent: "center", margin: "1rem"}}>
                                <img src={detail.img_url} className="card-img-top" />
                                <footer className="card-footer bg-white footer-position" style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    columnGap: ".5rem",
                                    minHeight: "4rem",
                                    }}>
                                    <div>
                                        <p className="card-title fw-bold text-secondary">{detail.name}:</p>
                                        <p className="card-text text-muted text-secondary">{detail.color} {detail.fabric.name} {detail.style.name}</p>
                                    </div>
                                    <div>
                                        <button className="btn btn-danger" onClick={deleteCard} value={detail.id} style={{position: "absolute", right: 0.5, margin: "5px"}}>Delete</button>
                                    </div>
                                </footer>
                            </div>
                            )
                        }
                    )}
                </div>
            </div>
        )
    }
    return response
}

export default function HatDetails() {
    const [cardData, setCardData] = useState([])

    async function deleteCard(event) {
        const id = event.target.value
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch(`http://localhost:8090/api/hats/${event.target.value}/`, fetchConfig)
        if (response.ok) {
            const copyData = [...cardData]
            setCardData(copyData.filter((card) => card.id != id))
        }

    }

    const fetchData = async () => {
        try {
            const response = await fetch("http://localhost:8090/api/hats/")
            if (response.ok) {
                const data = await response.json()

                const requests = []
                data.hats.forEach((item) => {
                    const url = `http://localhost:8090/api/hats/${item.id}/`
                    requests.push(fetch(url))
                })

                const responses = await Promise.all(requests)
                const responsesData = []
                responses.forEach(async (item) => {
                    const details = await item.json()
                    responsesData.push(details)
                })
                setTimeout(() => {
                    setCardData(responsesData)
                }, 1500)

            }
        } catch (e) {
            console.error(e)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])




    return(
        <div>
            <div className="px-4 py-5 my-5 mt-0 text-center">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                <h1 className="display-5 fw-bold">Hats for Every Occasion!</h1>
                <div className="col-lg-6 mx-auto">
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a hat!</Link>
                    </div>
                </div>
            </div>
            <hr />
            <div id="carouselExampleDark" className="carousel carousel-dark slide" data-bs-ride="carousel" data-bs-interval="false">
                <div className="carousel-inner">
                    {cardData ? DetailCards(cardData, deleteCard).map(item => item) : null}
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" id="left" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" id="right" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>

        </div>
    )
}
from django.contrib import admin
from .models import LocationVO, Hat, Fabric, Style

# Register your models here.
admin.site.register(LocationVO)
admin.site.register(Hat)
admin.site.register(Fabric)
admin.site.register(Style)
from django.urls import path
from .views import api_list_hats, api_list_styles, api_list_fabrics, api_hat_details, api_list_locationVO

urlpatterns = [
    path("hats/", api_list_hats, name="create_list_hats"),
    path("hats/<int:id>/", api_hat_details, name="details_delete_hats"),
    path("styles/", api_list_styles, name="create_list_styles"),
    path("fabrics/", api_list_fabrics, name="create_list_fabrics"),
    path("locations/", api_list_locationVO, name="create_list_locations"),
]

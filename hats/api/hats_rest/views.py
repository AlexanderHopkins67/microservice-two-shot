from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

from .models import LocationVO, Style, Fabric, Hat


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "import_href"
    ]


class StyleEncoder(ModelEncoder):
    model = Style
    properties = [
        "name",
        "id"
    ]


class FabricEncoder(ModelEncoder):
    model = Fabric
    properties = [
        "name",
        "id"
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "color",
        "img_url",
        "fabric",
        "style",
        "location"
    ]
    encoders = {
        "location": LocationVOEncoder(),
        "style": StyleEncoder(),
        "fabric": FabricEncoder()
    }


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            style = Style.objects.get(name=content["style"])
            fabric = Fabric.objects.get(name=content["fabric"])

            content["location"] = location
            content["style"] = style
            content["fabric"] = fabric
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

    else:
        hat = Hat.objects.all()
        return JsonResponse(
            {"hats": hat},
            encoder=HatListEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_hat_details(request, id):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Invalid ID"}, status=400)


@require_http_methods(["GET"])
def api_list_styles(request):
    styles = Style.objects.all()
    return JsonResponse({"styles": styles}, encoder=StyleEncoder, safe=False)


@require_http_methods(["GET"])
def api_list_fabrics(request):
    fabrics = Fabric.objects.all()
    return JsonResponse({"fabrics": fabrics}, encoder=FabricEncoder, safe=False)


@require_http_methods(["GET"])
def api_list_locationVO(request):
    locations = LocationVO.objects.all()
    return JsonResponse({"locations": locations}, encoder=LocationVOEncoder, safe=False)

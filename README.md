# Wardrobify

Team:

* Alex Hopkins - Hats
* Alec Weinstein-Shoes

## Design

## Shoes microservice

Shoes is a microservice that interfaces with the Wardrobe microservice through a poller that polls for Bins(where shoes are stored) every ten seconds and populates the Shoes microservice with BinVOs.

The shoes microservice contains a Shoe and BinVO model. Shoes have manufacaturers, model names,colors, a photo, and bin which is a foreign key to the BinVO model. The BinVO model contains an import href and a name.

## Hats microservice

To sufficiently encapsulate all the data related to a Hat object, I broke my model down into 3 value objects and 1 entity object. Of the value objects, my model contains one for Locations, one for Styles, and one for Fabrics. I thought each of these would benefit from being presented as a dropdown menu in the creation form, and by restricting option selection, perhaps aiding the construction of future features such as a search bar and/or filter. Everything else such as color, name, and image url, were added to the properties of the Hat entity object, with the additional VOs being foreign key properties.

Integration with the wardrobe is facilitated by a poller, which every 60 seconds queries the wardrobe api for the current list of valid locations, then either updates or creates a new value object for each location inside my microservice model database. In order to maintain an association with the location inside the master database, this VO accepts the "href" of the corresponding wardrobe location as a property labeled "import_href". These VOs are used to associate a hat with a specific location inside the microservice database via a foreign key, creating a many-to-one relationship and by extension associates it with a location inside the wardrobe database.

P.S. The hat detail carousel has some, lets call them fun, bugs, definitely not annoying or frustrating at all... Trying to get that to work with just React and Bootstrap was... limiting.